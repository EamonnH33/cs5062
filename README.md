# CS5062

A repository for sharing content from the University of Limerick module CS5062 Data Analytics.  
Work in progress. E-tivities included:  
- Etivity-1
- Etivity-2
- Etivity-3
- Etivity-4
- Etivity-5


## Getting started

You can clone this repo using the following command:  
```
git clone git@gitlab.com:malvido/CS5062.git
```
